class User < ActiveRecord::Base
  attr_accessible :username, :first_name, :last_name, :email, :serial_number, :password, :password_confirmation

  attr_accessor :password
  before_save :encrypt_password

  validates :password, :confirmation => true,
                       :presence => true, :on => :create,
                       :length => { :in => 8..20 }
  validates :username, :presence => true,
                       :uniqueness => true
  validates :first_name, :presence => true
  validates :last_name, :presence => true
  validates :email, :presence => true,
                    :uniqueness => true
  validates :serial_number, :presence => true,
                     :uniqueness => true
  has_many :resources
 
  def self.authenticate(username, password)
    user = find_by_username(username)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end

  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end

end
