class SessionsController < ApplicationController
  skip_before_filter :require_login
  
  def new
  end

  def create
    user = User.authenticate(params[:username], params[:password])
    if user
      session[:user_id] = user.id
      redirect_to root_url, :notice => "Logged in!"
    else
      flash.now.alert = "Invalid email or password"
      render "new"
    end
  end

  def destroy
    session[:user_id] = nil
    session[:resource_id] = nil
    redirect_to root_url, :notice => "Logged out!"
  end

  def add_resource_to_session
    session[:resource_id] = params[:id] if params[:id] != nil
  end
end
