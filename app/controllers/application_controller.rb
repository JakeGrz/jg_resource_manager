class ApplicationController < ActionController::Base
  protect_from_forgery
  helper_method :current_user
  helper_method :current_resource
  helper_method :add_resource_to_session
  before_filter :require_login
  
  private

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
  
  def require_login
    unless current_user
      redirect_to log_in_path
    end
  end
  
  def current_resource
    @current_resource ||= Resource.find(session[:resource_id]) if session[:resource_id]
  end

  def add_resource_to_session
    session[:resource_id] = params[:id] if params[:id] != nil
  end
  
  def require_resource
    unless current_resource
      redirect_to resources_path
      flash.now.alert = "Please Select a Resource!"
    end
  end
end
